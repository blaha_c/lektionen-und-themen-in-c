//
// Created by ArbeitsPC on 26.06.2021.
//
#include <stdio.h>
#include <stdlib.h>

void printArrayA(int *aArray){
    for(int i = 0; i < 5; i++)
        printf("arr[%1d]=%2d ", i, aArray[i]);
    printf("\n");
}
void printArrayB(int *aArray){
    for(int i = 0; i < 5; i++)
        printf("*arr+%1d=%2d ", i, *aArray+i);
    printf("\n");
}
void printMAa(int *aArray){
    //MA = Memory Address
    for(int i = 0; i < 5; i++)
        printf("<MA: &arr[%1d]=0x%x> ", i, &aArray[i]);
    printf("\n");
}
void printMAb(int *aArray){
    //MA = Memory Address
    for(int i = 0; i < 5; i++)
        printf("<MA: &arr+%1d=0x%x> ", i, &aArray+i);
    printf("\n");
}

int main() {
    //Is set at compile time.
    int a[] = {1, 2, 3, 4, 5};
    int b[5] = {6, 7, 8, 9, 10};
    int c[5];
    //Is set while compile time.
    //It is fine style to cast the return of [malloc() -> (void *)] to the target data type.
    int *d = (int *)malloc(5 * sizeof(int));

    for(int i = 0; i < 5; i++){
        c[i] = i + 11;
        d[i] = i + 16;
    }

    /*
    printArrayA(a);
    printArrayA(b);
    printArrayA(c);
    printArrayA(d);
    printArrayB(a);
    printArrayB(b);
    printArrayB(c);
    printArrayB(d);
     */
    /*
    printMAa(a);
    printMAa(b);
    printMAa(c);
    printMAa(d);
    printMAb(a);
    printMAb(b);
    printMAb(c);
    printMAb(d);
    */

    //No "&" necessary because the name of an array is a Pointer on the first element of this array.
    int *e = a;
    printArrayA(e);
    printf("%1d", *a);

    //Resource: https://www.geeksforgeeks.org/pointer-array-array-pointer/
    //Giving a Pointer the size of an array
    int *p = a;
    int (*ptr)[5] = &a;
    printf("p = %p, ptr = %p\n", p, ptr);
    printf("*p = %d, *ptr = %p\n", *p, *ptr);
    printf("sizeof(p) = %lu, sizeof(*p) = %lu\n", sizeof(p), sizeof(*p));
    printf("sizeof(ptr) = %lu, sizeof(*ptr) = %lu\n", sizeof(ptr), sizeof(*ptr));

    return 0;
}