//
// Created by ArbeitsPC on 26.06.2021.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    //empty string:
    char empty1[] = "";
    char empty2[] = {'\0'};

    //Is set at compile time.
    char s1[] = {'H', 'e', 'l', 'l', 'o', '1', '\0'};
    char s2[] = "Hello2";
    //It doesn't make a difference if size is bigger the "empty" space will be filled with '\0'
    char s3[20] = {'H', 'e', 'l', 'l', 'o', '3', '\0'};
    char s4[20] = "Hello4";
    char *s5 = "Hello5";
    //Copying string through copying a pointer
    char *s6 = s1;

    //Is set while compile time.
    char *s7 = malloc(7 * sizeof(char));
    s7[0] = 'H'; s7[1] = 'e'; s7[2] = 'l'; s7[3] = 'l'; s7[4] = 'o'; s7[5] = '6'; s7[6] = '\0';

    char s8[20];
    //1. "error: invalid array assignment"
    //2. It still works but compiler shows "warning: initialization makes integer from pointer without a cast"
    //3. during runtime if used "error: subscripted value is neither array nor pointer nor vector"
    //char s8 = s1;     //1.
    //char s9 = s1;     //2. & 3.
    //char s10 = *s1;   //3.

    if(s3[10] == '\0')  //also worked (s3[10 == '\0'])
        printf("s3[10] == '\\0'\n");

    printf("%s %s %s %s %s %s %s\n", s1, s2, s3, s4, s5, s6, s7);

    /*
     * Variants of String-Iteration:
     * Caution: s1 is set at compile time
     */
    int cnt = sizeof(s1)/sizeof(char);
    for(int i = 0; i < cnt; i++){
        printf("%c ", s1[i]);
    }
    printf("\n");
    //----------------------------------------
    for(int i = 0; i < strlen(s1); i++){
        printf("%c ", s1[i]);
    }
    printf("\n");
    //----------------------------------------
    for(int i = 0; s1[i] != '\0'; i++){
        printf("%c ", s1[i]);
    }
    printf("\n");
    //----------------------------------------
    int i = 0;
    while(s1[i]) {
        printf("%c ", s1[i]);
        i++;
    }
    printf("\n");
    //----------------------------------------
    char *str1 = s1;
    while(*str1) {
        printf("%c ", *str1);
        *str1++;
    }
    printf("\n");
    //----------------------------------------
    char *str2 = s1;
    char *str3 = s1;
    do {
        printf("%c ", *str3);
    } while (*str2++ = *str3++);
    printf("\n");

    return 0;
}