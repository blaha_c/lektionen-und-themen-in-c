//
// Created by ArbeitsPC on 26.06.2021.
//
#include <stdio.h>
int main() {
    //"*": Returns the referenced value of a pointer.
    //"&": Returns the memory address.
    //"%x": Prints numbers in the hexadecimal system.

    int a = 42;
    int *b = &a;
    int *c = b;
    int **d = &c;
    int ***e = &d;  //*(*(*e)) == ***e

    //int a saves an integer of 42.
    printf("<Value:    a =%3d> <Memory Address: &a = 0x%x>\n", a, &a);
    //int *b saves the address to variable a.
    printf("<Value:   *b =%3d> <Memory Address: &b = 0x%x> <Dump:   b = 0x%x>\n", *b, &b, b);
    //int *c saves the address to variable a.
    printf("<Value:   *c =%3d> <Memory Address: &c = 0x%x> <Dump:   c = 0x%x>\n", *c, &c, c);
    //int **d saves the address of variable c and know that c has an address leading to the value.
    printf("<Value:  **d =%3d> <Memory Address: &d = 0x%x> <Dump:  *d = 0x%x> <Dump:  d = 0x%x>\n", **d, &d, *d, d);
    //int ***e saves the address of variable d and know that d has an address to another address leading to the value.
    printf("<Value: ***e =%3d> <Memory Address: &e = 0x%x> <Dump: **e = 0x%x> <Dump: *e = 0x%x> Dump: e = 0x%x>\n", ***e, &e, **e, *e, e);

    return 0;
}