//
// Created by ArbeitsPC on 28.06.2021.
//
#ifndef LEKTIONEN_UND_THEMEN_IN_C_STORAGE_MANAGEMENT_H
#define LEKTIONEN_UND_THEMEN_IN_C_STORAGE_MANAGEMENT_H

extern void printArrayint10(int *aArray);
extern void printArraychar10(char *aArray);
extern void printArraydouble10(double *aArray);
extern char *testfree(void);

#endif //LEKTIONEN_UND_THEMEN_IN_C_STORAGE_MANAGEMENT_H