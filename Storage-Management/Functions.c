//
// Created by ArbeitsPC on 28.06.2021.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printArrayint10(int *aArray){
    for(int i = 0; i < 10; i++) {
        printf("%d ", aArray[i]);
    }
    printf("\n");
}

void printArraychar10(char *aArray){
    for(int i = 0; i < 10; i++) {
        printf("%c ", aArray[i]);
    }
    printf("\n");
}

void printArraydouble10(double *aArray){
    for(int i = 0; i < 10; i++) {
        printf("%d ", aArray[i]);
    }
    printf("\n");
}

//Does free() shorten a memory block?
char *testfree(void) {
    char *s1 = (char *) malloc(100 * sizeof(char));
    s1[0] = 'H'; s1[1] = 'e'; s1[2] = 'l'; s1[3] = 'l'; s1[4] = 'o'; s1[5] = '!'; s1[6] = '\0'; s1[7] = 'B'; s1[8] = '\0';
    printf("s1=%s; length=%d, sizeOfPointeraddress=%d Byte\n", s1, strlen(s1), sizeof(s1));
    printf("<%c>\n", s1[7]);
    /*
    free(s1);
    printf("s1=%s; length=%d, sizeOfPointeraddress=%d Byte\n", s1, strlen(s1), sizeof(s1));
    printf("<%c>\n", s1[7]);
    */
    /*
    s1 = (char *) realloc(s1, 7 * sizeof(char));
    printf("s1=%s; length=%d, sizeOfPointeraddress=%d Byte\n", s1, strlen(s1), sizeof(s1));
    printf("<%c>\n", s1[7]);
    */
    s1 = (char *) realloc(s1, 150 * sizeof(char));
    printf("s1=%s; length=%d, sizeOfPointeraddress=%d Byte\n", s1, strlen(s1), sizeof(s1));
    printf("<%c>\n", s1[7]);
    return s1;
}