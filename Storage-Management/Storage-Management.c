//
// Created by ArbeitsPC on 28.06.2021.
//
#include "Storage-Management.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main() {
    //Gerneral informations:
    //https://www.geeksforgeeks.org/dynamic-memory-allocation-in-c-using-malloc-calloc-free-and-realloc/
    //https://www.programiz.com/c-programming/c-dynamic-memory-allocation

    //Allocate memory
    //It is fine style to cast the return of malloc() -> (void *) to the target data type.
    int *ptr1 = (int *)malloc(10 * sizeof(int));
    char *ptr2 = (char *)malloc(10 * sizeof(char));
    printArrayint10(ptr1);
    printArraychar10(ptr2);
    if(ptr1 == NULL || ptr1 == NULL)
        return 1;

    //Same as malloc(), but It initializes each block with a default value ‘0’.
    int *ptr3 = (int *)calloc(10, sizeof(int));
    char *ptr4 = (char *)calloc(10, sizeof(char));
    printArrayint10(ptr3);
    printArraychar10(ptr4);
    if(ptr3 == NULL || ptr4 == NULL)
        return 1;

    //memset() is for working with strings
    //Caution: Not sizeof(prt1)
    //Resource: https://thecodingbot.com/a-detailed-tutorial-on-memset-in-c-c-with-usage-and-examples/
    memset(ptr1, 33, 10 * sizeof(int));
    memset(ptr2, 'z', 10 * sizeof(char));
    printArrayint10(ptr1);
    printArraychar10(ptr2);

    //Interpret same memory block as 5 double
    //Resource: https://straub.as/c-cpp-qt-fltk/c/malloc-calloc-realloc-free.html
    double *ptr5 = (double *)ptr1;
    printArraydouble10(ptr5);
    printf("Integer: %llu Byte Double: %llu Byte\n", sizeof(int), sizeof(double));

    //You have to give the total size of the memory not only the extended size.
    ptr1 = (int *)realloc(ptr1, 12 * sizeof(int));
    ptr2 = (char *)realloc(ptr2, 12 * sizeof(char));
    ptr3 = (int *)realloc(ptr3, 12 * sizeof(int));
    ptr4 = (char *)realloc(ptr4, 12 * sizeof(char));

    int *ptr7;
    ptr7 = (int *)realloc(ptr1, 14 * sizeof(int));
    ptr7[13] = 55;
    printf("%d\n", ptr7[13]);
    //Failure?????
    //ptr5 = (double *)realloc(ptr5, 10 * sizeof(double));
    double *ptr6 = (double *)malloc(2* sizeof(double));
    ptr6 = (double *)realloc(ptr6, 12 * sizeof(double));
    ptr6[11] = 5.5;
    printf("%f\n", ptr6[11]);

    //Does free() shorten a memory block?
    char *s1 = testfree();
    printf("s1=%s; length=%d, sizeOfPointeraddress=%d Byte\n", s1, strlen(s1), sizeof(s1));
    printf("<%c>\n", s1[7]);

    //free() works only with allocated memory during runtime.
    //warning: attempt to free a non-heap object 'arr'
    int arr[] = {1, 2, 3, 4, 5, 6 ,7 , 8, 9, 10};
    //free(arr);
    //We cant free ptr1, ptr5
    free(ptr2);
    free(ptr3);
    free(ptr4);
    free(ptr6);
    free(ptr7);

    return 0;
}