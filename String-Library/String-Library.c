//
// Created by ArbeitsPC on 27.06.2021.
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
    char origStr1[] = "My birthday is on 21.12.1998 and when is your birthday?";
    char origStr2[] = "31.11.1920 ";
    char *subStr1, *subStr2, *subStr3, *subStr4, *subStr5;
    char *newStr1, *newStr2, *newStr3;

    //int puts ( const char * str );
    //Write string to stdout:
    //Print String in Console with newline, in <stdio.h>
    printf("puts():    <");
    puts(origStr1);

    //size_t strlen ( const char * str );
    //Get string length:
    //Get the string length without '\0'.
    subStr3 = (char *)malloc((strlen(origStr1) - strlen(subStr1) + 1) * sizeof(char));
    strncpy(subStr3, origStr1, strlen(origStr1) - strlen(subStr1));
    subStr3[strlen(origStr1) - strlen(subStr1)] = '\0';
    printf("strlen():  <%s>\n", subStr3);

    //char * strcpy ( char * destination, const char * source );
    //Copy string:
    //Copies string from stated pointer. Needs allocated memory.
    subStr4 = (char *)malloc((strlen(subStr3) - 3 + 1) * sizeof(char));
    strcpy(subStr4, subStr3 + 3);
    subStr4[0] = 'B';
    //subStr4[strlen(subStr3) - 3] = '\0';
    printf("strcpy():  <%s>\n", subStr4);

    //char * strncpy ( char * destination, const char * source, size_t num );
    //Copy characters from string:
    //Copies a stated size of a String after a pointer. It is no '\0' copied. Needs allocated memory.
    subStr2 = malloc(5 * sizeof(char));
    if (subStr2 != NULL) {
        strncpy(subStr2, origStr2, 5);
    }
    printf("strncpy(): <%s>\n", subStr2);

    //char * strdup( const char * source );
    //Allocate a copy of a string up to specified size:
    //Copies string from stated pointer. Needs no allocated memory.
    newStr1 = strdup(subStr4);
    printf("strup():   <%s>\n", newStr1);

    //char * strstr ( const char *, const char * );
    //Locate substring:
    //Copies the rest of the string after its appearance. Needs no allocated memory.
    subStr1 = strstr(origStr1,"21");
    printf("strstr():  <%s>\n", subStr1);

    //char * strcat ( char * destination, const char * source );
    //Concatenate strings:
    //Concatenate two strings. Needs allocated memory.
    subStr4 = realloc(subStr4, (strlen(subStr4) + strlen(origStr2) + 1) * sizeof(char));
    strcat(subStr4, origStr2);
    subStr4 = realloc(subStr4, (strlen(subStr4) + strlen((origStr1 + 29)) + 1) * sizeof(char));
    strcat(subStr4, origStr1+29);
    printf("strcat():  <%s>\n", subStr4);

    //char * strncat ( char * destination, const char * source, size_t num );
    //Append characters from string:
    //Concatenate one String with a stated size of the second String. Needs allocated memory.
    newStr2 = (char *)malloc((strlen(origStr1) + 1) * sizeof(char));
    newStr2[0] = '\0';
    strncat(newStr2, subStr1+3, 15);
    printf("strncat(): <%s>\n", newStr2);

    //void * memset ( void * ptr, int value, size_t num );
    //Fill block of memory:
    //Fill memory with same character.
    memset(newStr2+8, '-', 7);
    printf("strncat(): <%s>\n", newStr2);

    //char * strtok ( char * str, const char * delimiters );
    //Split string into tokens:
    //Delimiter are in a string.
    printf("strtok():  <");
    subStr5 = strtok (newStr3," .");
    while (subStr5 != NULL){
        printf("%s-",subStr5);
        subStr5 = strtok (NULL, " .");
    }
    printf(">\n");

    //int strcmp ( const char * str1, const char * str2 );
    //Compare two strings:
    //Gives back 0 for equal or -1/1 depends on first letter is lower or greater.
    int i = strcmp(newStr1, newStr2);
    printf("strncat(): <%s> == <%s> -> %d\n", newStr1, newStr2, i);

    return 0;
}