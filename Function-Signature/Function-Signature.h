//
// Created by ArbeitsPC on 28.06.2021.
//
#ifndef LEKTIONEN_UND_THEMEN_IN_C_FUNCTION_SIGNATURE_H
#define LEKTIONEN_UND_THEMEN_IN_C_FUNCTION_SIGNATURE_H

extern int plus1(int num);
extern int *plus2(int num);
extern int plus3(int *num);
extern int *plus4(int *num);
extern int plus5(int num);
extern int *plus6(int num);
extern int plus7(int *num);
extern int *plus8(int *num);

extern void void1(int num);
extern void void2(void);
extern void void3();

#endif //LEKTIONEN_UND_THEMEN_IN_C_FUNCTION_SIGNATURE_H