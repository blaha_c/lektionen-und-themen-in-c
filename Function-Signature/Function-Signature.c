//
// Created by ArbeitsPC on 28.06.2021.
//
#include "Function-Signature.h"
#include <stdio.h>
#include <stdlib.h>
int main() {
    int a = 1, b = 1, c = 1, d = 1;
    int *e = &a, *f = &b, *g = &c, *h = &d;
    int s, t, u, v;
    int *w, *x, *y, *z;

    int arrA[] = {1, 1, 1, 1, 1};
    int arrB[] = {2, 2, 2, 2, 2};
    int *arrC = malloc(5 * sizeof(int));

    s = plus1(a);
    w = plus2(b);
    t = plus3(e);
    x = plus4(f);
    u = plus5(c);
    y = plus6(d);
    v = plus7(g);
    z = plus8(h);

    printf("plus1() = %d\n", s);
    printf("plus2() = %d\n", *w);
    printf("plus3() = %d\n", t);
    printf("plus4() = %d\n", *x);
    printf("plus5() = %d\n", u);
    printf("plus7() = %d\n", *y);
    printf("plus6() = %d\n", v);
    printf("plus8() = %d\n", *z);

    void1(1);
    void2();
    void3(1);

    return 0;
}