//
// Created by ArbeitsPC on 28.06.2021.
//
/*
 * Order factors for functions and signatures:
 * 1. "local" allocated memory(no|yes)
 * 2. input  (value|pointer) (Call by value|Call by reference)
 * 3. return (value|pointer)
*/
#include <stdlib.h>
#include <stdio.h>

int plus1(int num){
    num += 1;
    return num;
}

//Need to stand outside of plus2() because after finishing plus2() memory is deallocated.
int plus2a;
int *plus2(int num) {
    plus2a = num + 2;
    int *plus2b = &plus2a;
    return plus2b;
}

int plus3(int *num){
    *num += 3;
    return *num;
}

int *plus4(int *num){
    *num += 4;
    return num;
}

int plus5(int num){
    int *returnNum = (int *)malloc(sizeof(int));
    *returnNum = num + 5;
    return *returnNum;
}

int *plus6(int num){
    int *returnNum = (int *)malloc(sizeof(int));
    *returnNum = num + 6;
    return returnNum;
}
int plus7(int *num){
    int *returnNum = malloc(sizeof(int));
    *returnNum = *num + 7;
    return *returnNum;
}

int *plus8(int *num){
    int *returnNum = malloc(sizeof(int));
    *returnNum = *num + 8;
    return returnNum;
}

/*
 * ########## Void Functions ##########
 *
 * ### 1. No return/return data type ###
 * Functions without return value should not declared without return type like: function(int a){ ... }
 * If there is no return data type give, the compiler set int as default return data type.
 * 1. warning: return type defaults to 'int' [enabled by default]
 * 2. warning: 'return' with no value, in function returning non-void [enabled by default]
 * Without return-statement -> 1.
 * return 42; -> 1.
 * return; -> 1. & 2.
 *
 * ### 2. Is a return statement needed? ###
 * Void functions needn't any return statement. It should be left out.
 * A return statement makes only sense to break up a function premature.
 * For example in an if-else statement like void1().
 *
 * ### 3. No input/input data type ###
 * Functions without input value should not declared without input type like: int function(){ ... }
 * The function can receive all all data types.
 */

void void1(int num){
    if(num == 42) {
        printf("Void 1: %d\n", num);
        return;
    }
    printf("Void 1: %d\n", num);
}

void void2(void){
    int num = 42;
    printf("Void 3: %d\n", num);
}

void void3(){
    int num = 43;
    printf("Void 4: %d\n", num);
}