//
// Created by ArbeitsPC on 27.06.2021.
//
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void function(char *a, int charIndex){
    //The single pointer underneath don't lead to useful data.
    //printf("%c", *(a + 0) + 0);
    //Without cast it's an error.
    //printf("%c", *(*(a + 2) + charIndex));
    printf("%c", *(*((char **)a + 2) + charIndex));
    printf("%c\n", ((char **)a)[2][charIndex + 3]);
}

int main() {
    //Casting Pointer
    //implicit Cast
    /*
    int a = 42;
    int *b = &a;
    void *c = b;            //no (void *) necessary
    int *d = (int *)c;
    int e = *d;
    printf("%d\n", e);
    */

    //explicit cast
    int a = 33;             //33 = '!' (ASCII)
    //With the void-pointer we have an implicit cast
    //void *c = &a;
    int *c = &a;
    //It still works without (char *) but compiler shows "warning: initialization from incompatible pointer type"
    char *e = (char *)c;
    printf("%c\n", *e);

    //Useful trick for Pointer (char *) of multidimensional arrays
    char *f[] = {"Christian", " Martin", " Blaha."};
    char **g = f;
    char *h = (char *)g;
    //For the access of a string "%s" **f is forbidden!
    printf("%s %s %c%c", *f, *(f + 1) + 1, *(*(f + 2) + 1), *(*((char **)h + 2) + 2));
    //Example for the signature a pointer has to be casted.
    function((char *)f, 3);

    //Primitive Cast: char <-> integer
    int m = 33;
    char o = '9';
    char l = (char)m;
    int n = (int)o;
    int p = o + 0;
    printf("<%c> - <%d> - <%d>\n", l, n, p);

    //Simple Cast: char <-> natural numbers
    char str1[] = "04567";
    int y;
    int num1[] = {1, 6, 0, 4, 6};
    char z;
    for(int i = 0; i < 5; i++){
        y = str1[i] - '0';
        z = num1[i] + '0';
        printf("<%d> - <%c>\n", y, z);
    }

    //Better Cast: string -> integer
    char str2[] = "-21";
    char *str = str2;
    int isNegative = 1;
    int num = 0;
    while (*str){
        if (*str == '-') {
            isNegative = -1;
        }else {
            if (isdigit(*str)) {
                num = num*10 + *str - '0';
            }
        }
        str++;
    }
    num *= isNegative;
    printf("<%d>\n", num);

    //Correct Cast: string <-> integer
    char s1[10];
    int num2 = -15;
    char *s2 = "-16";
    itoa(num2, s1, 10);
    int num3 = atoi(s2);
    printf("<%s> - <%d>\n", s1, num3);

    return 0;
}