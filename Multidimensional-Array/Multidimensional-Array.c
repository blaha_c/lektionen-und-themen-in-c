//
// Created by ArbeitsPC on 27.06.2021.
//
#include <stdio.h>
void setZero(int b[][2][2]){
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            for (int k = 0; k < 2; k++) {
                b[i][j][k] = 0;
            }
        }
    }
}

void setNumbers(int b[][2][2]){
    int a = 1;
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            for (int k = 0; k < 2; k++) {
                b[i][j][k] = a++;
            }
        }
    }
}

void printArray(int b[][2][2]){
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            for (int k = 0; k < 2; k++) {
                printf("%d\t", b[i][j][k]);
            }
        }
    }
    printf("\n");
}

int main() {
    //2D-array
    //Resource: https://www.geeksforgeeks.org/pointer-array-array-pointer/
    int a[3][4] = { {1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12} };
    int b[3][4] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
    printf("a[2][2] = %2d\n", a[2][2]);
    printf("*(*(a + 2) + 2) = %2d\n", *(*(a + 2) + 2));
    printf("b[2][2] = %2d\n", b[2][2]);
    printf("*(*(b + 2) + 2) = %2d\n", *(*(b + 2) + 2));
    //Interval between rows:
    printf("a        = 0x%x, a + 1    = 0x%x, a + 2    = 0x%x, a + 3    = 0x%x\n", a, a + 1, a + 2, a + 3);
    printf("&a[0][0] = 0x%x, &a[1][0] = 0x%x, &a[2][0] = 0x%x, &a[3][0] = 0x%x\n", &a[0][0], &a[1][0], &a[2][0], &a[3][0]);
    //One Pointer in 2D-Array gives only a memory address back
    printf("*(a + 1) = 0x%x, *((a + 1) + 2) = 0x%2x, *a + 3 = 0x%2x\n", *(a + 3), *((a + 1) + 2), *a + 3);
    //Row
    printf("**a   = %2d, **(a + 1) = %2d, **(a + 2) = %2d, **(a + 3) = %2d\n", **a, **(a + 1), **(a + 2), **(a + 3));
    //Column (2. line also possible, becaus 2D-array is saved as line)
    printf("*(*a) = %2d, *(*a + 1) = %2d, *(*a + 2) = %2d, *(*a + 3) = %2d\n", *(*a), *(*a + 1), *(*a + 2), *(*a + 3));
    printf("**a   = %2d, **a + 1   = %2d, **a + 2   = %2d, **a + 3   = %2d\n", **a, **a + 1, **a + 2, **a + 3);
    //Row & Column (*(*(arr + row) + column)
    printf("**a   = %2d, *(*(a+1)+1) = %2d, *(*(a+2)+2) = %2d, *(*(a+2)+3) = %2d\n", **a, *(*(a + 1) + 1), *(*(a + 2) + 2), *(*(a + 2) + 3));

    //3D-array
    //Resource: https://www.geeksforgeeks.org/pointer-array-array-pointer/
    //Output:       a[0][0][0]; a[0][0][1]; a[0][1][0]; a[0][1][1]; a[1][0][0]; a[1][0][1]; a[1][1][0]; a[1][1][1];
    //Output order:      1           2           3           4           5           6           7           8
    int c[2][2][2];
    setNumbers(c);
    //c[i][j][k] == *(*(*(a + i) + j) + k)
    printf("%d = %d\n", c[1][1][1], *(*(*(c + 1) + 1) + 1));
    setZero(c);

    //-> c[i][j][k]
    //== *(c[i][j] + k)
    //== *(*(c[i] + j) + k)
    //== *(*(*(c + i) + j) + k)
    //If an index is 0, it is been dropped!
    //Invalid: *(*c[i] + k) + j -> It is only possible to add inside an address *()

    //Exampels:
    ***c = 1;           //1. element
    *(**c + 1) = 2;     //2. element
    *(*(*c+1) + 1) = 4; //4. element
    **c[1] = 5;         //5. element
    *(*c[1] + 1) = 6;   //6. element
    **(c[1] + 1) = 7;   //7. element
    *c[0][1] = 3;       //3. element
    c[1][1][1] = 8;     //8. element

    printArray(c);

    return 0;
}